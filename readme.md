# retrobanker

## v.01

A wingspan boardgame clone about banking. Most mechanics are stolen from wingspan. Test some concept and ideas around the retrobanker idea.

### sandbox
* add some sort of NFTs

### addons

* gs_logger
* gut

## todo

### programming
* [ ] add some sort of structs to pass data between modules
* [ ] add classes that can be used 

### testing
* [ ] loading a scene to test with GUT (godot unit test)

### GUI
* [ ] dynamic buttons for progress
* [ ] add icons for stock market type
* [ ] try global variable for GUI Score, update?

### animation
* [ ] animation for cards in hand
* [ ] animation when cards are moved to process
* [ ] startup animation
* [ ] about animation (with plot text)

### highscore
* [ ] save some highscore anywehere
* [ ] history graph of past seasons

### cards
* [ ] add special features to card
* [ ] add little history to card
* [ ] add 50 cards
* [ ] add 100 cards

### release
* [ ] remove itch.io loading times
* [ ] get some demoplayer (meetup, discord)

### gitlab ci
* [ ] automated windows build
* [ ] automated linux build
* [ ] automated osx build
