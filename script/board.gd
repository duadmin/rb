extends Node2D

var array: Array[int] = []

var Stockmarket = ["Energy", "Consumer", "Healthcare", "Financial", "Technology"]
var Values = ["Coins", "Options", "Credits"]
var legal_level = 0
var grey_level = 0
var illegal_level = 0

const TURNS = 5
const ROUNDS = 3


# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("progress/legal/legal_level_0").disabled = false
	get_node("progress/grey/grey_level_0").disabled = false
	get_node("progress/illegal/illegal_level_0").disabled = false
	
	get_node("Score/Money/Money").text = "{0}".format([0])
	get_node("Score/Credits/Credits").text = "{0}".format([0])
	get_node("Score/Options/Options").text = "{0}".format([0])
	get_node("Score/Coins/Coins").text = "{0}".format([0])
	get_node("Score/Turn/Turn").text = "{0}".format([1])
	get_node("Score/Round/Round").text = "{0}".format([1])
	get_node("Score/Turn/MaxTurn").text = "{0}".format([TURNS])
	get_node("Score/Round/MaxRound").text = "{0}".format([ROUNDS])
	
	get_node("Score/Energy/Energy").text = "{0}".format([10])
	get_node("Score/Consumer/Consumer").text = "{0}".format([20])
	get_node("Score/Healthcare/Healthcare").text = "{0}".format([30])
	get_node("Score/Financial/Financial").text = "{0}".format([20])
	get_node("Score/Technology/Technology").text = "{0}".format([10])
	
	cards.key.shuffle()
	self.add_card(3)

	Logger.set_logger_level(Logger.LOG_LEVEL_ALL)
	
	Logger.info("test info")



func card_pressed(name, coins, credits, options):
	Logger.info("play card:", name)
	var card = "Cards/{0}".format([name])
	if play_card(name):
		get_node(card).queue_free()
		if cards.CARDS.get(name)["Type"] == "legal":
			if legal_level < 4:
				get_node("progress/legal/legal_level_{0}".format([legal_level])).disabled = true
				get_node("progress/legal/legal_level_{0}".format([legal_level])).text = name 
				Logic.add("Legal", "Coins", coins.to_int())
				Logic.add("Legal", "Credits", credits.to_int())
				Logic.add("Legal", "Options", options.to_int())
				legal_level += 1
				get_node("progress/legal/legal_level_{0}".format([legal_level])).disabled = false
		if cards.CARDS.get(name)["Type"] == "grey":
			if grey_level < 4:
				get_node("progress/grey/grey_level_{0}".format([grey_level])).disabled = true
				get_node("progress/grey/grey_level_{0}".format([grey_level])).text = name 
				grey_level += 1
				Logic.Progress.get("Grey")["Coins"] += coins.to_int()
				Logic.Progress.get("Grey")["Credits"] += credits.to_int()
				Logic.Progress.get("Grey")["Options"] += options.to_int()
				get_node("progress/grey/grey_level_{0}".format([grey_level])).disabled = false
		if cards.CARDS.get(name)["Type"] == "illegal":
			if illegal_level < 4:
				get_node("progress/illegal/illegal_level_{0}".format([illegal_level])).disabled = true
				get_node("progress/illegal/illegal_level_{0}".format([illegal_level])).text = name 
				illegal_level += 1
				Logic.Progress.get("Illegal")["Coins"] += coins.to_int()
				Logic.Progress.get("Illegal")["Credits"] += credits.to_int()
				Logic.Progress.get("Illegal")["Options"] += options.to_int()
				get_node("progress/illegal/illegal_level_{0}".format([illegal_level])).disabled = false
				
		Logger.debug("{0}".format([Logic.Progress]))
		turn()


func play_card(name) -> bool:
	var cost = cards.CARDS.get(name)["Cost"]

	var returnvalue = true
	for stock in Stockmarket:
		if cost.has(stock):
			if cost[stock] > get_value("Score", stock):
				Logger.info("Missing {0} {1} stock".format([ cost[stock] - get_value("Score", stock), stock]))
				returnvalue = false
	
	if returnvalue:
		for stock in Stockmarket:
			if cost.has(stock):
				add_value("Stock", stock, cost[stock])
	return returnvalue



func add_card(number):

	for i in number:	
		if cards.key.size() > 0:
			var name = cards.key.pop_front()
			var button = Button.new()
			var vbox = VBoxContainer.new()
			var label = Label.new()
			var cost = Label.new()
			var credits = Label.new()
			var options = Label.new()
			var coins = Label.new()
			vbox.name = name
			button.text = name
			label.text = cards.CARDS.get(name)["Type"]
			for item in cards.CARDS.get(name)["Cost"]:
				cost.text += "{0}:{1} ".format([item.substr(0,1),cards.CARDS.get(name)["Cost"][item]])
			
			var credits_value = "{0}".format([cards.CARDS.get(name)["Credits"]])
			var coins_value = "{0}".format([cards.CARDS.get(name)["Coins"]])
			var options_value = "{0}".format([cards.CARDS.get(name)["Options"]])
			credits.text = credits_value
			options.text = options_value
			coins.text = coins_value

			vbox.add_child(button)
			vbox.add_child(label)
			vbox.add_child(cost)
			vbox.add_child(coins)
			vbox.add_child(credits)
			vbox.add_child(options)
			
			button.pressed.connect(self.card_pressed.bind(name, coins_value, credits_value, options_value))
			get_node("Cards").add_child(vbox)
		else:
			Logger.warn("no cards left")


func turn():
	var turn = get_value("Score", "Turn")
	if turn < TURNS:
		turn += 1
		set_value("Score", "Turn", turn)
	else:
		if get_value("Score", "Round") < ROUNDS:
			get_node("RoundFinished").visible = true
			get_node("RoundFinished").dialog_text = "{0} Quarter done, score is {1}".format([get_value("Score", "Round"), get_score()])
			turn = 1
			set_value("Score", "Turn", turn)
			next_round()
		else:
			get_node("GameFinished").visible = true
			get_node("GameFinished").dialog_text = "game over, score is {0}".format([get_score()])
			reset()

func next_round():
	var round = get_value("Score", "Round")
	if round <= ROUNDS:
		round += 1
	else:
		Logger.info("finish")
	set_value("Score", "Round", round)


func get_score() -> int:
	var score = get_value("Score", "Coins")
	score += get_value("Score", "Credits")
	score += get_value("Score", "Options")
	return score

func get_value(section, name) -> int:
	var node = "{0}/{1}/{1}".format([section, name])
	if has_node(node):
		return get_node("{0}/{1}/{1}".format([section, name])).text.to_int()
	else:
		Logger.error(node, " not found")
		return 0
	

func set_value(section, name, value):
	var node = "{0}/{1}/{1}".format([section, name])
	if has_node(node):
		get_node(node).text = "{0}".format([value])
	else:
		Logger.error(node, " not found")


func add_value(section, name, add):
	set_value(section, name, get_value(section, name) + add)


func reset():
	
	for value in Values:
		set_value("Score", value, 0)	
	
	for stock in Stockmarket:
		set_value("Stock", stock, 0)
		set_value("Score", stock, 0)
	set_value("Blub", "Turn", 1)
	set_value("Score", "Turn", 1)
	set_value("Score", "Round", 1)

	legal_level = 0
	grey_level = 0
	illegal_level = 0
	
	get_node("progress/legal/legal_level_{0}".format([legal_level])).disabled = false
	get_node("progress/grey/grey_level_{0}".format([grey_level])).disabled = false
	get_node("progress/illegal/illegal_level_{0}".format([illegal_level])).disabled = false
	
	get_node("progress/legal/legal_level_{0}".format([legal_level])).text = "{0}".format([0]) 
	get_node("progress/grey/grey_level_{0}".format([grey_level])).text = "{0}".format([0]) 
	get_node("progress/illegal/illegal_level_{0}".format([illegal_level])).text = "{0}".format([0])  
	
	for level in range(1,5):
		get_node("progress/legal/legal_level_{0}".format([level])).disabled = true
		get_node("progress/grey/grey_level_{0}".format([level])).disabled = true
		get_node("progress/illegal/illegal_level_{0}".format([level])).disabled = true
		
		get_node("progress/legal/legal_level_{0}".format([level])).text = "{0}".format([level]) 
		get_node("progress/grey/grey_level_{0}".format([level])).text = "{0}".format([level]) 
		get_node("progress/illegal/illegal_level_{0}".format([level])).text = "{0}".format([level])  
	
	Logic.reset()
	
	for child in get_node("Cards").get_children():
		child.queue_free()
		
	cards.reset()
	self.add_card(3)
	
### connections with buttons


func _on_legal_level_pressed():
	for stock in Stockmarket:
		add_value("Stock", stock, 1)
	add_value("Score", "Coins", Logic.Progress["Legal"]["Coins"])
	add_value("Score", "Credits", Logic.Progress["Legal"]["Credits"])
	add_value("Score", "Options", Logic.Progress["Legal"]["Options"])
	turn()


func _on_illegal_level_pressed():
	Logger.info("add card")
	add_card(1)
	add_value("Score", "Coins", Logic.Progress["Illegal"]["Coins"])
	add_value("Score", "Credits", Logic.Progress["Illegal"]["Credits"])
	add_value("Score", "Options", Logic.Progress["Illegal"]["Options"])
	turn()


func _on_grey_level_pressed():
	var money = get_node("Score/Credits/Credits").text.to_int()
	money += 1
	add_value("Score", "Coins", Logic.Progress["Grey"]["Coins"])
	add_value("Score", "Credits", Logic.Progress["Grey"]["Credits"])
	add_value("Score", "Options", Logic.Progress["Grey"]["Options"])
	get_node("Score/Credits/Credits").text = "{0}".format([money])
	turn()


func _on_buy_technology_pressed():
	if get_value("Stock", "Technology"):
		add_value("Score", "Technology", 1)
		add_value("Stock", "Technology", -1)


func _on_buy_energy_pressed():
	if get_value("Stock", "Energy"):
		add_value("Score", "Energy", 1)
		add_value("Stock", "Energy", -1)


func _on_buy_consumer_pressed():
	if get_value("Stock", "Consumer"):
		add_value("Score", "Consumer", 1)
		add_value("Stock", "Consumer", -1)

func _on_buy_healthcare_pressed():
	if get_value("Stock", "Healthcare"):
		add_value("Score", "Healthcare", 1)
		add_value("Stock", "Healthcare", -1)


func _on_buy_financial_pressed():
	if get_value("Stock", "Financial"):
		add_value("Score", "Financial", 1)
		add_value("Stock", "Financial", -1)
