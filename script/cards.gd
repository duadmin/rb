extends Node

var key = CARDS.keys()

# TODO remove this because it's just a example for the GUT
var _speed = 12.0

const CARDS = {
	"Fortune's\nFavor": {
		"Type": "legal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Economic\nBoom": {
		"Type": "legal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Market\nManipulation": {
		"Type": "legal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Fiscal\nPolicy": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Financial": 3,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Monopoly\nPower": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Inflationary\nSurge": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Healthcare": 2,
			"Financial": 3,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Stock Market\nRally": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Credit\nCrunch": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Technology": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Bankruptcy\nBlues": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Wealth\nRedistribution": {
		"Type": "grey",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Consumer": 1,
			"Financial": 3,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Currency\nExchange": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Tax\nEvasion": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Debt\nConsolidation": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Financial": 3,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Economic\nDownturn": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Technology": 4,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Investment\nStrategy": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Cash Flow\nChaos": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Corporate\nMerger": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Consumer": 1,
		},
		"Coins": 2,
		"Credits":2,
		
		"Options": 1,
		"Stock": 0,
	},
	"Trade\nEmbargo": {
		"Type": "illegal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		
		"Options": 1,
		"Stock": 0,
	},
	"Asset Bubble\nBurst": {
		"Type": "legal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Credits":2,
		"Options": 1,
		"Stock": 0,
	},
	"Interest\nRate Hike": {
		"Type": "legal",
		"Tags": ["Brutal", "Slow"],
		"Requirements": "",
		"Cost": {
			"Energy": 1,
			"Healthcare": 2,
		},
		"Coins": 2,
		"Options": 1,		
		"Credits":2,
	},
}


func reset():
	key = CARDS.keys()
	key.shuffle()

# TODO remove this because it's just a example for the GUT
func _ready():
	_speed = 32.1

# TODO remove this because it's just a example for the GUT
func get_value(value) -> int:
	return value

# TODO remove this because it's just a example for the GUT
func get_speed() -> float:
	return _speed

# TODO remove this because it's just a example for the GUT
func set_speed(speed):
	_speed = speed
