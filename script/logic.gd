extends Node

var Progress = {
	"Legal" : {
		"Coins":0,
		"Credits":0,
		"Options":0,
	},
	"Grey" : {
		"Coins":0,
		"Credits":0,
		"Options":0,
	},
	"Illegal" : {
		"Coins":0,
		"Credits":0,
		"Options":0,
	}
}

func add(area, section, value):
	Progress[area][section] += value

func reset():
	Progress = {
		"Legal" : {
			"Coins":0,
			"Credits":0,
			"Options":0,
		},
		"Grey" : {
			"Coins":0,
			"Credits":0,
			"Options":0,
		},
		"Illegal" : {
			"Coins":0,
			"Credits":0,
			"Options":0,
		}
	}
