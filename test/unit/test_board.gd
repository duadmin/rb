extends GutTest

var Board = load("res://script/board.gd")
var board = Board.new()


func test_start():
	assert_eq(board.legal_level, 0, "start value")
	assert_eq(board.grey_level, 0, "start value")
	assert_eq(board.illegal_level, 0, "start value")

	
func test_button_pressed():
	var Score = VBoxContainer.new()
	var Stock = VBoxContainer.new()
	var Technology_box = HBoxContainer.new()
	var Technology_text = RichTextLabel.new()
	Score.name = "Score"
	Stock.name = "Stock"
	Technology_box.name = "Technology"
	Technology_text.name = "Technology"
	Technology_box.add_child(Technology_text)
	Score.add_child(Technology_box.duplicate())
	Stock.add_child(Technology_box)
	board.add_child(Score)
	board.add_child(Stock)
	board.set_value("Score", "Technology", 1)
	board.set_value("Stock", "Technology", 1)
	board._on_buy_technology_pressed()
	assert_eq(board.get_value("Score", "Technology"), 2, "buy tech")
	assert_eq(board.get_value("Stock", "Technology"), 0, "buy tech")
	board._on_buy_technology_pressed()
	assert_eq(board.get_value("Score", "Technology"), 2, "buy tech")
	assert_eq(board.get_value("Stock", "Technology"), 0, "buy tech")
