extends GutTest

var Cards = load("res://script/cards.gd")
var cards = Cards.new()

func before_all():
	cards._ready()

func test_compare_sizes():
	assert_eq(cards.key.size(), cards.CARDS.size(), "compare size")

# TODO remove this because it's just a example for the GUT
func test_get_value():
	#var cards = Cards.new()
	assert_eq(cards.get_value(42), 42, "check get_value")

# TODO remove this because it's just a example for the GUT
func test_check_speed():
	assert_accessors(cards, 'speed', 32.1, 1.23)
