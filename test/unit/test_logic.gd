extends GutTest

var Logic = load("res://script/logic.gd")
var logic = Logic.new()

func test_start():
	assert_eq(logic.Progress["Legal"]["Coins"], 0, "start value")

func test_add_stuff():
	logic.add("Legal", "Coins", 2)
	assert_eq(logic.Progress["Legal"]["Coins"], 2, "added stuff")
	logic.add("Legal", "Coins", -2)
	assert_eq(logic.Progress["Legal"]["Coins"], 0, "removed stuff")

func test_reset():
	logic.reset()
	assert_eq(logic.Progress["Legal"]["Coins"], 0, "added stuff")
